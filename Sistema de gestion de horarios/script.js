// script.js
function login() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var emailMessage = document.getElementById('emailMessage');
    var passwordMessage = document.getElementById('passwordMessage');

    // Resetear mensajes anteriores
    emailMessage.innerHTML = '';
    passwordMessage.innerHTML = '';

    // Validar el correo electrónico
    if (!isValidEmail(username)) {
        emailMessage.innerHTML = 'Por favor, introduce un correo electrónico válido.';
    }

    // Validar la contraseña
    if (!isValidPassword(password)) {
        passwordMessage.innerHTML = 'La contraseña debe tener al menos 8 caracteres, una letra mayúscula, un número y un símbolo.';
    }

    // Aquí puedes continuar con el proceso de inicio de sesión si todo está validado.
    if (isValidEmail(username) && isValidPassword(password)) {
        console.log('Usuario:', username);
        console.log('Contraseña:', password);
    }
}

function isValidEmail(email) {
    // Utilizar una expresión regular simple para verificar el formato del correo electrónico.
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

function isValidPassword(password) {
    // La contraseña debe tener al menos 8 caracteres, una letra mayúscula, un número y un símbolo.
    var passwordRegex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return passwordRegex.test(password);
}
function login() {
    // Lógica de inicio de sesión aquí

    // Redirigir al home
    window.location.href = "home/home.html";
}
