// Datos de ejemplo para la primera tabla
let horarios = [
    { dia: 'Lunes', hora: '10:00 AM - 12:00 PM', materia: 'Matemáticas', profesor: 'Dr. Rodríguez' },
    // Puedes agregar más horarios aquí
];

// Datos de ejemplo para la segunda tabla
let otraTabla = [
    { dia: 'Miércoles', hora: '2:00 PM - 4:00 PM', materia: 'Historia', profesor: 'Lic. Pérez' },
    // Puedes agregar más horarios aquí
];

function cargarTabla(tableId, data) {
    const table = document.getElementById(tableId);
    const tbody = document.createElement('tbody');

    data.forEach((item) => {
        const row = tbody.insertRow();
        row.innerHTML = `<td>${item.dia}</td>
                         <td>${item.hora}</td>
                         <td>${item.materia}</td>
                         <td>${item.profesor}</td>
                         <td>
                             <button onclick="editarHorario('${tableId}', ${data.indexOf(item)})">Editar</button>
                             <button onclick="eliminarHorario('${tableId}', ${data.indexOf(item)})">Eliminar</button>
                         </td>`;
    });

    // Limpiar la tabla antes de agregar una nueva
    table.innerHTML = '';

    table.appendChild(tbody);
}

function agregarHorario(tableId) {
    const nuevoHorario = {
        dia: prompt('Ingrese el día del horario'),
        hora: prompt('Ingrese la hora del horario'),
        materia: prompt('Ingrese la materia del horario'),
        profesor: prompt('Ingrese el nombre del profesor'),
    };

    if (tableId === 'horarioTable') {
        horarios.push(nuevoHorario);
        cargarTabla('horarioTable', horarios);
    } else if (tableId === 'otraTabla') {
        otraTabla.push(nuevoHorario);
        cargarTabla('otraTabla', otraTabla);
    } else {
        // Nueva lógica para manejar la tabla recién creada
        const nuevaTabla = document.getElementById(tableId);
        const tbody = nuevaTabla.createTBody();

        const row = tbody.insertRow();
        row.innerHTML = `<td>${nuevoHorario.dia}</td>
                         <td>${nuevoHorario.hora}</td>
                         <td>${nuevoHorario.materia}</td>
                         <td>${nuevoHorario.profesor}</td>
                         <td>
                             <button onclick="editarHorario('${tableId}', 0)">Editar</button>
                             <button onclick="eliminarHorario('${tableId}', 0)">Eliminar</button>
                         </td>`;
    }
}

function crearNuevaTabla() {
    const nuevaTablaId = prompt('Ingrese un ID para la nueva tabla');
    const nuevaTablaCard = document.createElement('div');
    nuevaTablaCard.className = 'card';
    nuevaTablaCard.id = `${nuevaTablaId}Card`;

    const nuevaTablaSection = document.createElement('section');
    const nuevaTablaTitle = document.createElement('h2');
    nuevaTablaTitle.textContent = 'Nueva Tabla';

    const nuevaTablaTable = document.createElement('table');
    nuevaTablaTable.id = nuevaTablaId;

    const nuevaTablaButton = document.createElement('button');
    nuevaTablaButton.textContent = 'Agregar Horario';
    nuevaTablaButton.onclick = function () {
        agregarHorario(nuevaTablaId);
    };

    nuevaTablaSection.appendChild(nuevaTablaTitle);
    nuevaTablaSection.appendChild(nuevaTablaTable);
    nuevaTablaSection.appendChild(nuevaTablaButton);

    nuevaTablaCard.appendChild(nuevaTablaSection);

    document.getElementById('mainContainer').insertBefore(nuevaTablaCard, document.getElementById('mainContainer').lastElementChild);
}

function editarHorario(tableId, index) {
    // Lógica para editar un horario existente
    console.log(`Función para editar horario en la tabla ${tableId}, índice ${index}`);
    // Puedes agregar más lógica según tus necesidades
}

function eliminarHorario(tableId, index) {
    // Lógica para eliminar un horario existente
    console.log(`Función para eliminar horario en la tabla ${tableId}, índice ${index}`);
    // Puedes agregar más lógica según tus necesidades
}

// Cargar los horarios al cargar la página
cargarTabla('horarioTable', horarios);
cargarTabla('otraTabla', otraTabla);
