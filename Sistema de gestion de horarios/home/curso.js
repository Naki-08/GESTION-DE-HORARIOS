// script.js

let contadorCursos = 2; // Contador inicial para mantener un seguimiento único de los cursos

function agregarCurso() {
    contadorCursos++;
    
    const nuevoCurso = document.createElement('div');
    nuevoCurso.className = 'curso';
    nuevoCurso.id = 'curso' + contadorCursos; // Asigna un ID único al nuevo curso

    nuevoCurso.innerHTML = `
        <h3>Nombre del Curso</h3>
        <p>Profesor: Nombre del Profesor</p>
        <p>Horario: Días y Horas</p>
        <p>Alumnos Inscritos: 0</p>
        <button onclick="editarCurso('${nuevoCurso.id}')">Editar Curso</button>
    `;
    
    document.querySelector('section').insertBefore(nuevoCurso, document.querySelector('button'));
}

function editarCurso(cursoId) {
    const nombre = prompt('Ingrese el nuevo nombre del curso');
    const profesor = prompt('Ingrese el nuevo nombre del profesor');
    const horario = prompt('Ingrese el nuevo horario del curso');

    const curso = document.getElementById(cursoId);
    curso.innerHTML = `
        <h3>${nombre}</h3>
        <p>Profesor: ${profesor}</p>
        <p>Horario: ${horario}</p>
        <p>Alumnos Inscritos: 0</p>
        <button onclick="editarCurso('${cursoId}')">Editar Curso</button>
    `;
}
function redirectTo(relativePath) {
    // Obtener la ruta actual del navegador
    var currentPath = window.location.pathname;

    // Eliminar el nombre del archivo actual de la ruta
    var newPath = currentPath.substring(0, currentPath.lastIndexOf('/'));

    // Construir la nueva URL
    var newUrl = newPath + '/' + relativePath;

    // Redirigir
    window.location.href = newUrl;
}
