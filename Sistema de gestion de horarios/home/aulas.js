// aulas.js

let contadorAulas = 0; // Contador para asignar IDs únicos a las aulas

function agregarAula() {
    contadorAulas++;

    const nombreAula = prompt('Ingrese el nombre del aula:');
    const ubicacionAula = prompt('Ingrese la ubicación del aula:');
    const capacidadAula = parseInt(prompt('Ingrese la capacidad del aula:'));

    const nuevaFila = document.createElement('tr');
    nuevaFila.innerHTML = `
        <td>${contadorAulas}</td>
        <td>${nombreAula}</td>
        <td>${ubicacionAula}</td>
        <td>${capacidadAula}</td>
        <td>
            <button onclick="editarAula(this)">Editar</button>
        </td>
    `;

    document.querySelector('table tbody').appendChild(nuevaFila);
}

function editarAula(botonEditar) {
    const fila = botonEditar.parentNode.parentNode;
    const nombreActual = fila.children[1].textContent;
    const ubicacionActual = fila.children[2].textContent;
    const capacidadActual = parseInt(fila.children[3].textContent);

    const nuevoNombre = prompt('Editar nombre del aula:', nombreActual);
    const nuevaUbicacion = prompt('Editar ubicación del aula:', ubicacionActual);
    const nuevaCapacidad = parseInt(prompt('Editar capacidad del aula:', capacidadActual));

    fila.children[1].textContent = nuevoNombre;
    fila.children[2].textContent = nuevaUbicacion;
    fila.children[3].textContent = nuevaCapacidad;
}
